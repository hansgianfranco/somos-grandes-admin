import axios from 'axios';
import { getLocalStorageItem } from "@/helpers/localStorage";

const headers = {
  Accept: "application/json, text/plain, */*",
  "Content-Type": "application/json",
};

const accessToken = getLocalStorageItem('sgAccessToken');

export const getBusiness = async () => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/business?deleted=true`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const getBusinessById = async (uuid: string) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/business/${uuid}`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const updateBusinessByStatus = async (uuid: string, values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/api/business/${uuid}/status`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};