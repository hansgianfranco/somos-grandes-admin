import axios from 'axios';
import { getLocalStorageItem } from "@/helpers/localStorage";

const headers = {
  Accept: "application/json, text/plain, */*",
  "Content-Type": "application/json",
};

const accessToken = getLocalStorageItem('sgAccessToken');

export const getContentSections = async () => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/content/sections`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const getContentById = async (uuid: string) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/contents/${uuid}`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const getContentChapterById = async (uuid: string) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/api/content/chapters/${uuid}`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const postContent = async (values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/contents`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const postContentSection = async (values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/content/sections`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const postContentChapter = async (values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/content/chapters`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const updateContentSectionById = async (uuid: string, values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/api/content/sections/${uuid}`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const updateContentChapterById = async (uuid: string, values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/api/content/chapters/${uuid}`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const updateContentById = async (uuid: string, values: any) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/api/contents/${uuid}`, values, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const deleteContentSection = async (uuid: string) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.delete(`${process.env.NEXT_PUBLIC_API_URL}/api/content/sections/${uuid}`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};

export const deleteContentChapter = async (uuid: string) => {
  const config = {
    headers: {
      ...headers,
      Authorization: `Bearer ${accessToken}`,
    },
  }
  try {
    const response = await axios.delete(`${process.env.NEXT_PUBLIC_API_URL}/api/content/chapters/${uuid}`, config);
    return response.data;
  } catch (error) {
    console.error(error);
    return {};
  }
};