import { getLocalStorageItem } from "@/helpers/localStorage"
import axios, { AxiosProgressEvent } from 'axios';

const headers = {
  Accept: "application/json"
}

const accessToken = getLocalStorageItem("sgAccessToken")

export const uploadVideoFile = async (file: File, onProgress: (progressEvent: AxiosProgressEvent) => void): Promise<any> => {
  const formdata = new FormData();
  formdata.append("file", file);

  const config = {
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: onProgress,
  };

  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/upload/video`, formdata, config);
    return response.data;
  } catch (error: any) {
    if (error.response) {
      const errorData = error.response.data;
      throw new Error(errorData.error.message || 'Error al subir el archivo');
    } else {
      throw new Error('Error de red al subir el archivo');
    }
  }
};


export const uploadSingleFile = async (file: File) => {
  const formdata = new FormData();
  formdata.append("file", file);
  const config = {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  }

  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/upload/single`, formdata, config);
    return response.data;
  } catch (error) {
    throw new Error("Error de red al subir el archivo");
  }
};

export const uploadUserFile = async (file: File, onProgress: (progressEvent: AxiosProgressEvent) => void): Promise<any> => {
  const formdata = new FormData();
  formdata.append("file", file);
  
  const config = {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: onProgress
  };

  try {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/users/upload`, formdata, config);
    return response.data;
  } catch (error: any) {
    if (error.response) {
      const errorData = error.response.data;
      throw new Error(errorData.error.message || 'Error al subir el archivo');
    } else {
      throw new Error('Error de red al subir el archivo');
    }
  }
};