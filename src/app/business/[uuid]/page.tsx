'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import Slider from "react-slick";
import Avatar from '@/assets/images/profile.jpg'
import Header from '@/components/shared/header';
import { getBusinessById } from '@/api/business';
import LockBusinessModal from '@/components/modals/lockBusinessModal';
import DeleteBusinessModal from '@/components/modals/deleteBusinessModal';
import { IconDescription, IconMission, IconChallenge, IconPride, IconGoal, IconLink, IconEmail, IconFacebook, IconLinkedin, IconInstagram, IconX, IconPhone, IconMap } from '@/app/utils/icons';

interface EditProps {
    params: { uuid: string }
}

const BusinessEdit: React.FC<EditProps> = ({ params }: EditProps) => {

    const statusLabels: any = {
        ACTIVE: 'Activa',
        INACTIVE: 'Inactiva',
        LOCKED: 'Bloqueada',
        DELETED: 'Eliminada',
    };

    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    const [activeTab, setActiveTab] = useState('about');
    const [business, setBusiness] = useState<any>(null);
    const [openLockModal, setOpenLockModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);

    const handleTabClick = (tabName: string) => {
        setActiveTab(tabName)
    }

    const handleBlockModal = (action: boolean) => {
        setOpenLockModal(action)
    }

    const handleDeleteModal = (action: boolean) => {
        setOpenDeleteModal(action)
    }

    const fetchBusinessById = async () => {
        const response = await getBusinessById(params.uuid)
        setBusiness(response.data)
    }

    useEffect(() => {
        fetchBusinessById()
    }, [])

    return (
        <>
            <main className="bg-white">
                <Header />
                {business && (
                    <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pb-[80px]">
                        <div className="h-[287px] w-full overflow-hidden rounded-bl-[16px] bg-[#d5c7e8] rounded-br-[16px]">
                            { business.business_photos?.length > 0 && (
                                <Slider {...settings}>
                                    {business.business_photos.map((photo: any, index: number) => (
                                        <div key={index} className="relative w-full h-[287px]">
                                            <Image className="object-cover" fill src={photo.photo_url} alt="" />
                                        </div>
                                    ))}
                                </Slider>
                            )}
                        </div>
                        <div className="relative flex justify-between items-center mt-[-40px] mb-[22px] z-1">
                            <div className="mt-[-15px] flex items-center gap-[8px] mb-[12px] ml-[68px]">
                                <div className="rounded-full overflow-hidden w-[165px] h-[165px] border-white border-[5px]  bg-[#d5c7e8]">
                                    { business.logo_url && (
                                        <Image className="block w-full h-full object-cover" fill src={business.logo_url} alt="" />
                                    )}
                                </div>
                                <h3 className="text-[24px] mt-[10px] text-[#410B9A] font-bold">{business.name}</h3>
                            </div>
                            {business.status !== "DELETED" && (
                                <div className="flex gap-[16px]">
                                    {business.status !== "LOCKED" && (
                                        <button
                                            className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] border border-[#410B9A] text-[#410B9A]"
                                            onClick={() => handleBlockModal(true)}
                                        >
                                            Bloquear
                                        </button>
                                    )}
                                    <button
                                        className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] text-white bg-gradient-to-r from-[#6C0BE8] to-[#191D2F]"
                                        onClick={() => handleDeleteModal(true)}
                                    >
                                        Dar de baja
                                    </button>
                                </div>
                            )}
                        </div>

                        <div className="flex justify-between">
                            <div className="w-full pr-[33px]">

                                <div className="flex gap-[70px] mb-[30px]">
                                    <button className={`text-[16px] leading-[24px] ${activeTab === 'about' ? 'text-[#6D04F2] border-b-[2px] border-[#6D04F2]' : 'text-[#D5C7E8]'} px-[13px]`} onClick={() => handleTabClick('about')}>Acerca de la empresa</button>
                                    <button className={`text-[16px] leading-[24px] ${activeTab === 'contact' ? 'text-[#6D04F2] border-b-[2px] border-[#6D04F2]' : 'text-[#D5C7E8]'} px-[13px]`} onClick={() => handleTabClick('contact')}>Contacto</button>
                                </div>

                                {activeTab === 'about' && (
                                    <div className="w-full grid grid-cols-2 gap-[24px] mb-[32px]">
                                        <div className="w-full px-[12px] rounded-[8px] py-[16px] box-shadow">
                                            <div className="flex justify-between mb-[4px]">
                                                <h4 className="text-[#410B9A] text-[14px] font-medium leading-[21px]">Descripción de la empresa</h4>
                                                <Image className="w-[23px] h-[23px]" src={IconDescription} alt="" />
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.about}</p>
                                        </div>
                                        <div className="w-full px-[12px] rounded-[8px] py-[16px] box-shadow">
                                            <div className="flex justify-between mb-[4px]">
                                                <h4 className="text-[#410B9A] text-[14px] font-medium leading-[21px]">Misión de la empresa</h4>
                                                <Image className="w-[22px] h-[24px]" src={IconMission} alt="" />
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.mission}</p>
                                        </div>
                                        <div className="w-full px-[12px] rounded-[8px] py-[16px] box-shadow">
                                            <div className="flex justify-between mb-[4px]">
                                                <h4 className="text-[#410B9A] text-[14px] font-medium leading-[21px]">Retos de la empresa</h4>
                                                <Image className="w-[33px] h-[33px]" src={IconChallenge} alt="" />
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.challenge}</p>
                                        </div>
                                        <div className="w-full px-[12px] rounded-[8px] py-[16px] box-shadow">
                                            <div className="flex justify-between mb-[4px]">
                                                <h4 className="text-[#410B9A] text-[14px] font-medium leading-[21px]">Orgullo empresarial</h4>
                                                <Image className="w-[23px] h-[28px]" src={IconPride} alt="" />
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.pride}</p>
                                        </div>
                                        <div className="w-full px-[12px] rounded-[8px] py-[16px] box-shadow">
                                            <div className="flex justify-between mb-[4px]">
                                                <h4 className="text-[#410B9A] text-[14px] font-medium leading-[21px]">Metas de la empresa</h4>
                                                <Image className="w-[30px] h-[26px]" src={IconGoal} alt="" />
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.goals}</p>
                                        </div>
                                    </div>
                                )}
                                {activeTab === 'contact' && (
                                    <>
                                        <div className="w-full flex mb-[32px]">
                                            <div className="w-1/2">
                                                <div className="flex items-center">
                                                    <Image className="w-[16px] h-[16px] mr-[8px]" src={IconLink} alt="" />
                                                    <h4 className="text-[#410B9A] text-[14px] leading-[21px]">Sitio web</h4>
                                                </div>
                                                <p className="text-[#69657E] text-[16px] leading-[24px]">{business.website_url}</p>
                                            </div>
                                            <div className="w-1/2">
                                                <div className="flex items-center">
                                                    <Image className="w-[16px] h-[16px] mr-[8px]" src={IconEmail} alt="" />
                                                    <h4 className="text-[#410B9A] text-[14px] leading-[21px]">Correo electrónico</h4>
                                                </div>
                                                <p className="text-[#69657E] text-[16px] leading-[24px]">{business.email}</p>
                                            </div>
                                        </div>
                                        <div className="w-full flex mb-[32px]">
                                            <div className="w-1/2">
                                                <p className="text-[#410B9A] text-[14px] leading-[21px] mb-[4px]">Redes sociales</p>
                                                <div className="flex items-end gap-[40px]">
                                                    { business.facebook_url && (
                                                        <div className="flex items-center">
                                                            <Image className="w-[16px] h-[16px] mr-[8px]" src={IconFacebook} alt="" />
                                                            <span className="text-[#69657E] text-[16px] leading-[24px]">{business.facebook_url}</span>
                                                        </div>
                                                    )}
                                                    { business.linkedin_url && (
                                                        <div className="flex items-center">
                                                            <Image className="w-[17px] h-[17px] mr-[8px]" src={IconLinkedin} alt="" />
                                                            <span className="text-[#69657E] text-[16px] leading-[24px]">{business.linkedin_url}</span>
                                                        </div>
                                                    )}
                                                    { business.instagram_url && (
                                                        <div className="flex items-center">
                                                            {/* <Image className="w-[17px] h-[17px] mr-[8px]" src={IconInstagram} alt="" /> */}
                                                            <span className="text-[#69657E] text-[16px] leading-[24px]">@{business.instagram_url}</span>
                                                        </div>
                                                    )}
                                                    { business.twitter_url && (
                                                        <div className="flex items-center">
                                                            <Image className="w-[17px] h-[17px] mr-[8px]" src={IconX} alt="" />
                                                            <span className="text-[#69657E] text-[16px] leading-[24px]">{business.twitter_url}</span>
                                                        </div>
                                                    )}
                                                </div>

                                            </div>
                                            <div className="w-1/2">
                                                <div className="flex items-center">
                                                    <Image className="w-[16px] h-[16px] mr-[8px]" src={IconPhone} alt="" />
                                                    <h4 className="text-[#410B9A] text-[14px] leading-[21px]">Teléfono</h4>
                                                </div>
                                                <p className="text-[#69657E] text-[16px] leading-[24px]">{business.phone}</p>
                                            </div>
                                        </div>
                                        <div className="w-full mb-[32px]">
                                            <div className="flex items-center">
                                                <Image className="w-[16px] h-[16px] mr-[8px]" src={IconMap} alt="" />
                                                <h4 className="text-[#410B9A] text-[14px] leading-[21px]">Ubicación</h4>
                                            </div>
                                            <p className="text-[#69657E] text-[16px] leading-[24px]">{business.address}</p>
                                        </div>
                                    </>
                                )}
                            </div>

                            <div className="w-full max-w-[264px] h-[268px] border-l border-l-[#6D04F2]">
                                <div className="pt-[10px] pl-[24px]">
                                    <p className="text-[#B1ADC8] text-[14px] leading-[21px] mb-[8px]">Usuario que dió de alta</p>
                                    <div className="flex items-center mb-[48px]">
                                        <div className="rounded-full overflow-hidden w-[25px] h-[25px] border-[#F2C930] border-[2px] mr-[8px]">
                                            <Image className="block" width={25} height={25} src={Avatar} alt="" />
                                        </div>
                                        <p className="text-[#353344] text-[14px] mb-0">Mauricio García</p>
                                    </div>
                                </div>
                                <div className="pl-[24px] mb-[63px]">
                                    <p className="text-[#B1ADC8] text-[14px] mb-[4px]">Fecha de alta</p>
                                    <p className="text-[#353344] text-[14px]">02/11/23</p>
                                </div>
                                <button className={`${business.status === "ACTIVE" ? 'bg-[#2E8F00]' : 'bg-[#69657E]'}  rounded-tr-[8px] rounded-br-[8px] text-white py-[5px] px-[35px] text-[16px] leading-[24px]`}>{statusLabels[business.status]}</button>
                            </div>
                        </div>
                    </div>
                )}
            </main>

            {openLockModal && (
                <LockBusinessModal uuid={params.uuid} callback={() => {
                    handleBlockModal(false)
                    fetchBusinessById()
                }} />
            )}
            {openDeleteModal && (
                <DeleteBusinessModal uuid={params.uuid} callback={() => {
                    handleDeleteModal(false)
                    fetchBusinessById()
                }} />
            )}
        </>
    );
};

export default BusinessEdit;