'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import Link from 'next/link';
import Header from '@/components/shared/header';
import { getBusiness } from '@/api/business';
import { format } from 'date-fns';
import oops from '@/assets/images/oops.svg'
import dog from '@/assets/images/dog.jpg'

const BusinessPage: React.FC = () => {

    const [loading, setLoading] = useState(true)
    const [business, setBusiness] = useState([])

    const statusLabels: any = {
        ACTIVE: 'Activa',
        INACTIVE: 'Inactiva',
        LOCKED: 'Bloqueada',
        DELETED: 'Eliminada',
    };

    const currentDate = new Date();
    const currentTime = format(currentDate, 'hh:mm a');

    const fetchBusiness = async () => {
        const response = await getBusiness()
        setBusiness(response.data)
        setLoading(false)
    }

    useEffect(() => {
        fetchBusiness()
    }, [])

    return (
        <div className="bg-white">
            <Header />
            <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                <div className="flex justify-between items-center mb-[55px]">
                    <div className="flex gap-[40px]">
                        <Link href="/business" className="text-[24px] text-[#410B9A] px-[13px] font-bold border-b-[2px] border-[#410B9A]">Empresas</Link>
                        <Link href="/leads" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Iniciativas</Link>
                        <Link href="/contents" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Contenido</Link>
                        <Link href="/users" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Usuarios</Link>
                    </div>
                    <div>
                        <p className="text-[18px] text-[#3A1477] font-bold">{business?.length} Empresas</p>
                        <p className="text-[12px] text-[#B1ADC8]">Actualizado {currentTime}</p>
                    </div>
                </div>
                { !loading && (
                    business?.length > 0 ? (
                        <div className="grid grid-cols-[repeat(auto-fit,222px)] gap-[40px] pb-[40px]">
                            {business?.map((item: any) => (
                                <Link key="item.id" href={`/business/${item.id}`}>
                                    <figure className="max-w-[222px] w-full h-full overflow-hidden rounded-[16px] box-shadow">
                                        <div className="relative h-[116px] bg-[#d5c7e8]">
                                            {item.business_photos[0] && (
                                                <Image className="block object-cover" fill src={item.business_photos[0]?.photo_url} alt="" />
                                            )}
                                            <span className={`absolute top-0 right-0 rounded-bl-[16px] rounded-tr-[16px] ${item.status === "ACTIVE" ? 'bg-[#2E8F00]' : 'bg-[#69657E]'} px-[16px] py-[4px] text-[10px] text-white font-bold`}>
                                                {statusLabels[item.status]}
                                            </span>
                                        </div>
                                        <figcaption className="relative px-[16px] pb-[15px] z-[1]">
                                            <div className="mt-[-15px] flex items-center gap-[8px] mb-[12px]">
                                                <div className="relative rounded-full overflow-hidden w-full max-w-[49px] h-[49px] border-[#F2C930] border-[2px] bg-[#d5c7e8]">
                                                    { item.logo_url && (
                                                        <Image className="block w-full h-full object-cover" fill src={item.logo_url} alt="" />
                                                    )}
                                                </div>
                                                <h3 className="text-[12px] leading-[12px] mt-[10px] text-[#410B9A] font-bold">{item.name}</h3>
                                            </div>
                                            <div className="px-[2px]">
                                                <div className="flex justify-start gap-[8px] mb-[6px]">
                                                    <div className={`relative rounded-full bg-[#d5c7e8] ${item.status === "ACTIVE" ? '' : 'grayscale opacity-50'} overflow-hidden min-w-[25px] h-[25px] border-[#F2C930] border-[2px]`}>
                                                        { item.user.profile_picture_url && (
                                                            <Image className="block" fill src={item.user.profile_picture_url} alt="" />
                                                        )}
                                                    </div>
                                                    <div className="">
                                                        <p className={`${item.status === "ACTIVE" ? 'text-[#353344]' : 'text-[#B1ADC8]'} text-[12px] leading-[12px]`}>{item.user.name + " " + item.user.last_name}</p>
                                                        <p className="text-[#B1ADC8] text-[10px]">80%</p>
                                                    </div>
                                                </div>
                                                <div className="mb-[12px]">
                                                    <p className="text-[#B1ADC8] text-[10px]">Actividad desde:</p>
                                                    <p className={`${item.status === "ACTIVE" ? 'text-[#353344]' : 'text-[#B1ADC8]'} text-[12px]`}>{format(new Date(item.created_at), 'dd/MM/yyyy')}</p>
                                                </div>
                                                { item.address && (
                                                    <div>
                                                        <p className="text-[#B1ADC8] text-[10px]">Ubicación</p>
                                                        <p className={`${item.status === "ACTIVE" ? 'text-[#353344]' : 'text-[#B1ADC8]'} text-[12px]`}>{item.address}</p>
                                                    </div>
                                                )}
                                                
                                            </div>
                                        </figcaption>
                                    </figure>
                                </Link>
                            ))}
                        </div>
                    ) : (
                        <div className="flex flex-col p-[60px] items-center justify-center">
                            <div className="mb-[24px]">
                                <Image src={oops} alt="" />
                            </div>
                            <div className="mb-[24px] text-center">
                                <h3 className="text-[24px] font-bold text-[#410B9A] mb-[8px]">Aún no se ha dado de alta ninguna empresas</h3>
                                <p className="text-[16px] text-[#353344]">Explicación / Sugerencia</p>
                            </div>
                            <div>
                                <Image src={dog} alt="" />
                            </div>
                        </div>
                    )
                )}
            </div>
        </div>
    );
};

export default BusinessPage;