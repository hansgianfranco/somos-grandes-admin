'use client'

import React, { useEffect, useRef, useState } from 'react';
import Image from "next/image";
import { useRouter } from 'next/navigation';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import Header from '@/components/shared/header';
import { getContentChapterById, updateContentChapterById } from '@/api/contents';
import { useDropzone } from 'react-dropzone';
import VideoJS from '@/components/shared/video';
import DeleteChapterModal from '@/components/modals/deleteChapterModal';
import { IconMedia, IconCloudUpload, IconSave, IconReturn } from '@/app/utils/icons';
import mime from 'mime';
import { uploadVideoFile } from '@/api/upload';

interface CreateProps {
    params: { uuid: string }
}

const validationSchema = Yup.object().shape({
    title: Yup.string().required(),
    author: Yup.string().required(),
    description: Yup.string().required(),
});

const EditChapterPage: React.FC<CreateProps> = ({ params }) => {
    const router = useRouter();

    const [files, setFiles] = useState<any>([]);
    const [chapter, setChapter] = useState<any>(null);
    const [thumbnails, setThumbnails] = useState<any>([]);
    const [selectedThumbnail, setSelectedThumbnail] = useState<any>(null);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [uploadProgress, setUploadProgress] = useState(0);
    const [uploadStatus, setUploadStatus] = useState('idle');

    const formikRef = useRef<any>(null);

    const handleDeleteModal = (action: boolean) => {
        setOpenDeleteModal(action)
    }

    const { getRootProps, getInputProps } = useDropzone({
        accept: {
            'video/*': []
        },
        onDrop: async (acceptedFiles: any) => {
            setUploadProgress(0)
            const file = acceptedFiles[0];
            const mimeType = mime.getType(file.name);
            const correctedFile = new File([file], file.name, { type: mimeType || 'application/octet-stream' });
            setFiles(acceptedFiles.map((file: any) => Object.assign(file, {
                preview: URL.createObjectURL(correctedFile)
            })));
            
            const responseMedia = await uploadVideoFile(correctedFile, (progressEvent: any) => {

                const progress = Math.round((progressEvent.loaded * 98) / progressEvent.total);
                setUploadProgress(progress);
            });
  
            if (responseMedia.status) {
                setThumbnails(responseMedia.data.thumbnails);
                setSelectedThumbnail(responseMedia.data.thumbnails[0])
                setUploadProgress(100);

                if (formikRef.current) {
                    formikRef.current.setFieldValue('media_url', responseMedia.data.video);
                    formikRef.current.setFieldValue('thumb_url', responseMedia.data.thumbnails[0]);
                }
            }
    
        }
    });

    const playerRef = React.useRef(null);

    const handlePlayerReady = (player: any) => {
        playerRef.current = player;
        player.on('waiting', () => {
        });

        player.on('dispose', () => {
        });
    };

    const thumbs = files.map((file: any) => {
        const videoOptions = {
            autoplay: false,
            controls: false,
            responsive: true,
            aspectRatio: '16:9',
            sources: [{
                src: file.preview,
                type: 'video/mp4'
            }]
        };
        return (
            <VideoJS key={file.name} options={videoOptions} onReady={handlePlayerReady} />
        )
    });

    useEffect(() => {
        return () => files.forEach((file: any) => URL.revokeObjectURL(file.preview));
    }, []);

    useEffect(() => {
        if (uploadProgress === 100 && uploadStatus === 'uploading') {
            setUploadStatus('success');
            setTimeout(() => {
                setUploadStatus('replace');
            }, 5000);
        } else if (uploadProgress < 100 && uploadProgress > 0 && uploadStatus !== 'uploading') {
            setUploadStatus('uploading');
        }
    }, [uploadProgress, uploadStatus]);



    const handleSubmit = async (values: any) => {
        const response = await updateContentChapterById(params.uuid, values);
        if (response.status) {
            router.push(`/contents/${chapter.content_id}`);
        }
    };

    const fetchChapterById = async () => {
        const response = await getContentChapterById(params.uuid)
        setChapter(response.data)
        setFiles([{ preview: response.data.media_url }])
    }

    useEffect(() => {
        fetchChapterById()
    }, [])

    return (
        <>
            <main className="bg-white">
                <Header />
                <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                    {chapter && (
                        <Formik
                            initialValues={{ author: chapter.author, title: chapter.title, description: chapter.description }}
                            isInitialValid={false}
                            validationSchema={validationSchema}
                            onSubmit={handleSubmit}
                            innerRef={formikRef}
                        >
                            {({ values, isValid }) => (
                                <Form>
                                    <div className="flex justify-between">
                                        <div className="w-1/2 max-w-[505px]">
                                            <h3 className="text-[16px] text-[#410B9A] leading-[24px] font-bold mb-[32px]">Editar capítulo</h3>
                                            <div className="mb-[36px]">
                                                <Field
                                                    name="title"
                                                    className="border border-[#E3E2EC] pl-[18px] w-full h-[56px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                                    type="text"
                                                    placeholder="Ingresa un título para el capítulo"
                                                />
                                            </div>
                                            <div className="mb-[36px]">
                                                <Field
                                                    name="author"
                                                    className="border border-[#E3E2EC] pl-[18px] w-full h-[56px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                                    type="text"
                                                    placeholder="Ingresa el nombre del autor"
                                                />
                                            </div>
                                            <div className="mb-[32px]">
                                                <div {...getRootProps({ className: 'dropzone w-full min-h-[245px] bg-[#D9D9D9] overflow-hidden rounded-[8px]' })}>
                                                    <input {...getInputProps()} />
                                                    {files.length > 0 ? (
                                                        <div className="relative pointer-events-none">
                                                            {thumbs}
                                                            <div className="absolute top-0 left-0 flex flex-col bg-black/[0.5] min-h-[245px] w-full h-full items-center justify-center">
                                                                <Image className="block mb-[10px] w-[102px] h-[102px]" src={IconMedia} alt="" />
                                                                <p className="text-[18px] text-white leading-[27px] font-medium">Reemplazar video</p>
                                                            </div>
                                                        </div>
                                                    ) : (
                                                        <div className="flex flex-col min-h-[245px] w-full h-full items-center justify-center">
                                                            <Image className="block mb-[10px]" src={IconCloudUpload} alt="" />
                                                            <p className="text-[18px] text-white leading-[27px] font-medium">Agregar video</p>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                            <div className="mb-[34px]">
                                                <Field
                                                    name="description"
                                                    as="textarea"
                                                    className="border border-[#E3E2EC] p-[18px] w-full h-[181px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                                    placeholder="Ingresa una descripción para que los usuarios sepan de qué trata el cápitulo"
                                                />
                                            </div>
                                        </div>
                                        <div className="w-1/2 max-w-[505px]">
                                            <div className="flex justify-center gap-[8px] mb-[30px]">
                                                <button
                                                    type="button"
                                                    className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] border border-[#410B9A] text-[#410B9A]"
                                                    onClick={() => handleDeleteModal(true)}
                                                >
                                                    Eliminar
                                                </button>
                                                <button
                                                    type="submit"
                                                    className={`w-[238px] h-[42px] flex justify-center items-center gap-[8px] block rounded-[40px] ${isValid ? 'bg-[#410B9A] text-white cursor-pointer' : 'bg-gray-400 text-white cursor-not-allowed'}`}
                                                    disabled={!isValid}
                                                >
                                                    <Image src={IconSave} alt="" />
                                                    Guardar cambios
                                                </button>
                                            </div>
                                            <div className="bg-[#F8F7F7] rounded-[16px] px-[33px]  py-[22px]">
                                                <div className="flex items-start">
                                                    <div className="w-full max-w-[139px]">
                                                        <h3 className="text-[16px] text-[#410B9A] leading-[24px] font-bold mb-[6px]">Vista previa</h3>
                                                        <p className="text-[14px] text-[#69657E] leading-[21px]">Así lo verán los empresarios</p>
                                                    </div>
                                                    <div className="w-[222px] h-[468px] mx-auto px-[5px] pr-[6px] py-[5px] bg-preview mb-[25px]">
                                                        <div className="w-full h-full rounded-[12px] overflow-hidden bg-[#EDEDED]">
                                                            <div className="relative h-[84px] bg-preview-title">
                                                                <div className="absolute top-[0] bottom-[30px] left-[15px] right-[15px] flex items-center gap-[5px] z-[1] text-[11px] text-white leading-[15px] font-medium">
                                                                    <Image src={IconReturn} alt="" />
                                                                    <span>{values.title}</span>
                                                                </div>
                                                            </div>
                                                            <div className="relative h-[404px] mt-[-30px] bg-white rounded-tl-[18px] rounded-tr-[18px] py-[17px] px-[15px] z-1">
                                                                <div className="w-full min-h-[100px] bg-[#EEEEEE] overflow-hidden rounded-[9px] mb-[10px]">
                                                                    {thumbs}
                                                                </div>
                                                                <p className="text-[9px] text-[#353344] leading-[13px] font-bold mb-[8px]">1. {values.title}</p>
                                                                <p className="text-[7px] text-[#69657E] leading-[12px] mb-[10px]">{values.author}</p>

                                                                <div className="w-full flex border-b-[#B1ADC8] border-b mb-[13px]">
                                                                    <div className="w-1/2 p-[6px] flex justify-center">
                                                                        <p className="text-[8px] text-[#B1ADC8] leading-[12px] font-bold">Capitulos</p>

                                                                    </div>
                                                                    <div className="w-1/2 p-[6px] flex justify-center">
                                                                        <p className="text-[8px] text-[#410B9A] leading-[12px] font-bold">Descripción</p>
                                                                    </div>
                                                                </div>
                                                                <p className="text-[8px] h-[60px] text-[#69657E] leading-[12px] mb-[11px]">{values.description}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3 className="text-[14px] text-[#410B9A] leading-[21px] font-medium mb-[6px]">Elige la portada de tu video</h3>
                                                <div className="relative flex bg-[#E9E9E9] h-[63px] rounded-[10px] overflow-hidden">
                                                    {thumbnails.length > 0 && thumbnails.map((thumb: string, index: number) => (
                                                        <button key={index} type="button" onClick={() => setSelectedThumbnail(thumb)} className={`thumb-image relative w-[99px] h-[63px] rounded-[10px] overflow-hidden ${selectedThumbnail === thumb ? 'border-[3px] border-[#6D04F2] opacity-100 !z-[10]' : 'opacity-60'} hover:opacity-100 hover:z-1`}>
                                                            <Image fill src={thumb} className="w-[99px]" alt="" />
                                                        </button>
                                                    ))}
                                                    {thumbnails.length === 0 && (
                                                        <div className="flex justify-center items-center w-full h-full">
                                                            <p className="text-[14px] text-[#B1ADC8] leading-[21px] font-bold">Agrega un video</p>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    )}
                </div>
            </main>
            {chapter && openDeleteModal && (
                <DeleteChapterModal uuid={chapter.id} callback={() => {
                    handleDeleteModal(false)
                    router.push(`/contents/${chapter.content_id}`);
                }} />
            )}
        </>
    );
};

export default EditChapterPage;