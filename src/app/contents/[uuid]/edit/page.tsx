'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import { useRouter } from 'next/navigation';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import Header from '@/components/shared/header';
import { getContentById, updateContentById } from '@/api/contents';
import { useDropzone } from 'react-dropzone';
import { uploadSingleFile } from '@/api/upload';
import { IconAddBanner, IconSave } from '@/app/utils/icons';

interface CreateProps {
    params: { uuid: string }
}

const validationSchema = Yup.object().shape({
    banner_url: Yup.string().required(),
    title: Yup.string().required(),
    description: Yup.string().required(),
});

const BusinessPage: React.FC<CreateProps> = ({ params }) => {
    const router = useRouter();

    const [files, setFiles] = useState<any>([]);
    const [content, setContent] = useState<any>(null);
    const { getRootProps, getInputProps } = useDropzone({
        accept: {
            'image/*': []
        },
        onDrop: (acceptedFiles: any) => {
            setFiles(acceptedFiles.map((file: any) => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
        }
    });

    const thumbs = files.map((file: any, index: number) => {
        return (
            <div key={index} className="relative w-full h-full">
                <Image
                    src={file.preview}
                    fill={true}
                    className="block w-auto h-full object-cover"
                    onLoad={() => { URL.revokeObjectURL(file.preview) }}
                    alt=""
                />
            </div>
        )
    });

    const fetchContentById = async () => {
        const response = await getContentById(params.uuid)
        setContent(response.data)
        setFiles([{preview: response.data.banner_url}])
    }

    useEffect(() => {
        fetchContentById()
    }, [])

    useEffect(() => {
        return () => files.forEach((file: any) => URL.revokeObjectURL(file.preview));
    }, []);


    const handleSubmit = async (values: any) => {
        if(files[0].name){
            const responseMedia = await uploadSingleFile(files[0]);
            values.banner_url = responseMedia.data
        }
        const response = await updateContentById(params.uuid, values);
        if (response.status) {
            router.push(`/contents/${params.uuid}`);
        }
    };

    return (
        <>
            <main className="bg-white">
                <Header />
                <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                    {content && (
                        <Formik
                            initialValues={{ banner_url: content.banner_url, title: content.title, description: content.description }}
                            validationSchema={validationSchema}
                            onSubmit={handleSubmit}
                        >
                            {({ values }) => (
                                <Form>
                                    <div className="flex justify-between">
                                        <div className="w-1/2 max-w-[505px]">
                                            <h3 className="text-[16px] text-[#410B9A] leading-[24px] font-bold mb-[32px]">Editar tema</h3>
                                            <div className="mb-[36px]">
                                                <Field
                                                    name="title"
                                                    className="border border-[#E3E2EC] pl-[18px] w-full h-[56px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                                    type="text"
                                                    placeholder="Ingresa un título para el capítulo"
                                                />
                                            </div>
                                            <div className="mb-[32px]">
                                                <div {...getRootProps({ className: 'dropzone w-full h-[245px] border-[#E3E2EC] border-2 border-dashed overflow-hidden rounded-[8px]' })}>
                                                    <input {...getInputProps()} />
                                                    {files.length > 0 ? thumbs : (
                                                        <div className="flex flex-col w-full h-full items-center justify-center">
                                                            <Image className="block mb-[10px]" src={IconAddBanner} alt="" />
                                                            <p className="text-[12px] text-[#6D04F2] leading-[18px] font-medium">Agregar  portada</p>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                            <div className="mb-[34px]">
                                                <Field
                                                    name="description"
                                                    as="textarea"
                                                    className="border border-[#E3E2EC] p-[18px] w-full h-[181px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                                    placeholder="Ingresa una descripción para que los usuarios sepan de qué trata el cápitulo"
                                                />
                                            </div>
                                        </div>
                                        <div className="w-1/2 max-w-[505px]">
                                            <div className="flex justify-center gap-[8px] mb-[30px]">
                                                <button
                                                    type="button"
                                                    className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] border border-[#410B9A] text-[#410B9A]"
                                                    onClick={() => router.push(`/contents/${content.id}`)}
                                                >
                                                    Cancelar
                                                </button>
                                                <button
                                                    type="submit"
                                                    className="w-[238px] h-[42px] flex justify-center gap-[8px] items-center block rounded-[40px] text-white bg-gradient-to-r from-[#6C0BE8] to-[#191D2F]"
                                                >
                                                    <Image src={IconSave} alt="" />
                                                    Guardar tema
                                                </button>
                                            </div>
                                            <div className="bg-[#F8F7F7] rounded-[16px] px-[33px] py-[22px]">
                                                <h3 className="text-[16px] text-[#410B9A] leading-[24px] font-bold mb-[32px]">Vista previa</h3>
                                                <p className="text-[14px] text-[#69657E] leading-[21px] font-bold mb-[32px]">Así lo verán los empresarios</p>
                                                <div className="w-[222px] h-[468px] mx-auto px-[5px] pr-[6px] py-[5px] bg-preview">
                                                    <div className="w-full h-full rounded-[12px] overflow-hidden bg-[#EDEDED]">
                                                        <div className="relative h-[234px]">
                                                            {thumbs}
                                                            <span className="absolute bottom-[40px] left-[15px] right-[15px] z-1 text-[14px] text-white leading-[21px] font-medium">{values.title}</span>
                                                        </div>
                                                        <div className="relative h-[254px] mt-[-30px] bg-white rounded-tl-[25px] py-[26px] px-[15px] rounded-tr-[25px] z-1">
                                                            <p className="text-[12px] text-[#410B9A] leading-[18px] font-bold mb-[8px]">Description:</p>
                                                            <p className="text-[10px] h-[60px] text-[#69657E] leading-[15px] mb-[11px]">{values.description}</p>

                                                            <div className="w-full px-[16px] py-[10px] border-b-[3px] border-b-[#6C0BE8] mb-[25px]">
                                                                <p className="text-[12px] text-[#410B9A] leading-[18px] font-bold">Capitulos</p>
                                                            </div>
                                                            <div className="w-full h-[300px] bg-[#EEEEEE] rounded-[8px]"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    )}
                </div>
            </main>
        </>
    );
};

export default BusinessPage;