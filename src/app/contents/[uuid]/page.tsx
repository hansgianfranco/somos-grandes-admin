'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import { useRouter } from 'next/navigation';
import Header from '@/components/shared/header';
import { getContentById } from '@/api/contents';
import { format } from 'date-fns';
import VideoJS from '@/components/shared/video';
import DeleteChapterModal from '@/components/modals/deleteChapterModal';
import { timeFormat } from '@/helpers/utils';
import MenuContentModal from '@/components/modals/menuContentModal';
import { IconEdit, IconDelete, IconAddChapter } from '@/app/utils/icons';

interface EditProps {
    params: { uuid: string }
}

const ContentPage: React.FC<EditProps> = ({ params }) => {
    const router = useRouter();

    const [content, setContent] = useState<any>(null);
    const [activeChapter, setActiveChapter] = useState<any>(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const [videoOptions, setVideoOptions] = useState({});
    const [openDeleteModal, setOpenDeleteModal] = useState(false);

    const handleDeleteModal = (action: boolean) => {
        setOpenDeleteModal(action)
    }

    const playerRef = React.useRef(null);

    const handlePlayerReady = (player: any) => {
        playerRef.current = player;
        player.on('waiting', () => {
        });

        player.on('dispose', () => {
        });
    };

    const handleChapterSelect = (chapter: any, index: number) => {
        setActiveChapter(chapter)
        setActiveIndex(index)
        setVideoOptions({
            autoplay: false,
            controls: true,
            responsive: true,
            aspectRatio: '16:9',
            sources: [{
                src: chapter.media_url,
                type: 'video/mp4'
            }]
        })
    }

    const fetchContentById = async () => {
        const response = await getContentById(params.uuid)
        setContent(response.data)
        if(response.data?.content_chapters?.length > 0){
            setActiveChapter(response.data.content_chapters[0])
            setVideoOptions({
                autoplay: true,
                controls: true,
                responsive: true,
                aspectRatio: '16:9',
                sources: [{
                    src: response.data.content_chapters[0].media_url,
                    type: 'video/mp4'
                }]
            })
        }
    }

    useEffect(() => {
        fetchContentById()
    }, [])

    return (
        <>
            <main className="bg-white">
                <Header />
                {content && (
                    <>
                        <div className="relative h-[287px] overflow-hidden">
                            <Image className="absolute top-0 left-0 h-full block w-full object-cover" fill src={content.banner_url} alt="" />
                            <div className="relative container max-w-[1024px] xl:max-w-[1280px] mx-auto z-1 pt-[185px]">

                                <div className="border-l border-l-white px-[16px]">
                                    <h2 className="text-[40px] text-white leading-[52px] font-medium">{content.title}</h2>
                                </div>
                            </div>
                            <MenuContentModal
                                addCallback={() => {
                                    router.push(`/sections/${content.content_section_id}/contents/create`);

                                }}
                                editCallback={() => {
                                    router.push(`/contents/${content.id}/edit`);

                                }}
                                deleteCallback={() => {
                                }}
                            />
                        </div>
                        <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pb-[80px]">

                            <div className="text-[16px] text-[#69657E] leading-[24px] py-[24px] border-b border-b-[#B1ADC8] mb-[40px]">
                                <p>{content.description}</p>
                            </div>

                            <div className="flex justify-between">

                                <div className="max-w-[505px] w-full">
                                    {activeChapter && (
                                        <>
                                            <div className="mb-[32px]">
                                                <h3 className="text-[24px] text-[#410B9A] leading-[32px] font-bold mb-[8px]">Capítulo: {activeChapter.title}</h3>
                                                <p className="text-[16px] text-[#B1ADC8] leading-[24px]">{activeChapter.author}</p>
                                            </div>
                                            <div className="bg-black max-w-[505px] rounded-[16px] w-full overflow-hidden mb-[32px]">
                                                <VideoJS options={videoOptions} onReady={handlePlayerReady} />
                                            </div>

                                            <div className="flex justify-between items-center pb-[30px] mb-[24px] border-b border-b-[#B1ADC8]">
                                                <div className="flex gap-[10px]">
                                                    <span className="inline-block border-[#410B9A] border-2 rounded-[12px] text-[#410B9A] text-[12px] leading-[18px] font-medium px-[12px] py-[8px]">
                                                        {content.content_section.title}
                                                    </span>
                                                </div>
                                                <button
                                                    className="flex items-center justify-center w-[45px] h-[45px] bg-[#D5C7E8] rounded-full"
                                                    onClick={() => {
                                                        router.push(`/chapters/${activeChapter.id}/edit`);
                                                    }}
                                                >
                                                    <Image src={IconEdit} alt="" />
                                                </button>
                                            </div>

                                            <div className="mb-[24px]">
                                                <p className="text-[15px] text-[#B1ADC8] leading-[24px] mb-[8px]">{format(activeChapter.created_at, "'Publicado el' dd MMM yyyy 'a las' HH:mm 'hr'")}</p>
                                                <div className="text-[14px] text-[#69657E] leading-[21px]">
                                                    <p>{activeChapter.description}</p>
                                                </div>
                                            </div>
                                            <button
                                                className="w-[239px] h-[40px] flex justify-center items-center gap-[8px] block rounded-[40px] text-[14px] text-[#410B9A] leading-[22px] border border-[#6D04F2]"
                                                onClick={() => handleDeleteModal(true)}
                                            >
                                                <Image src={IconDelete} alt="" />
                                                Eliminar  capítulo
                                            </button>
                                        </>

                                    )}
                                </div>


                                <div className="w-full max-w-[440px] pl-[24px] border-l-[2px] border-l-[#6D04F2]">
                                    <div className="flex items-center justify-between mb-[25px]">
                                        <p className="text-[16px] text-[#410B9A] leading-[24px] font-bold">{content.content_chapters_total} Capítulos</p>
                                    </div>
                                    <button
                                        className="w-full border-[#B1ADC8] border border-dashed rounded-[16px] flex justify-center items-center gap-[8px] text-[14px] text-[#B1ADC8] leading-[22px] p-[30px] mb-[24px]"
                                        onClick={() => router.push(`/contents/${content.id}/chapters/create`)}>
                                        <Image src={IconAddChapter} alt="" />
                                        Agregar nuevo capítulo
                                    </button>
                                    <div className="w-full"> 
                                        {content.content_chapters?.length > 0 && content.content_chapters.map((item: any, index: number) => (
                                            <button
                                                key={item.id}
                                                className={`w-full flex ${activeIndex === index ? 'bg-[#410B9A]' : ''} gap-[17px] p-[8px] text-left rounded-[8px] box-shadow mb-[24px]`}
                                                onClick={() => handleChapterSelect(item, index)}
                                            >
                                                <div className="relative w-full max-w-[105px] h-[65px] overflow-hidden rounded-[8px]">
                                                    <Image className="w-full h-full object-cover" fill src={item.thumb_url} alt="" />
                                                </div>
                                                <div className="w-full ">
                                                    <h4 className={`text-[14px] ${activeIndex === index ? 'text-white' : 'text-[#353344]'} leading-[21px] font-bold`}>{item.title}</h4>
                                                    <p className={`text-[12px] ${activeIndex === index ? 'text-white' : 'text-[#B1ADC8]'} leading-[18px]`}>{item.author}</p>
                                                    <p className={`text-[10px] ${activeIndex === index ? 'text-white' : 'text-[#410B9A]'} leading-[15px] font-bold`}>{item.media_time && timeFormat(item.media_time)}</p>
                                                </div>
                                            </button>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                )}
            </main>

            {activeChapter && openDeleteModal && (
                <DeleteChapterModal uuid={activeChapter.id} callback={() => {
                    handleDeleteModal(false)
                    fetchContentById()
                }} />
            )}
        </>
    );
};

export default ContentPage;