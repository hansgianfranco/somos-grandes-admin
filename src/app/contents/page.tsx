'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import Slider from "react-slick";
import Link from 'next/link';
import { format } from 'date-fns';

import Header from '@/components/shared/header';
import EditSectionModal from '@/components/modals/editSectionModal';
import { deleteContentSection, getContentSections } from '@/api/contents';

import AddSectionModal from '@/components/modals/addSectionModal';
import MenuSectionModal from '@/components/modals/menuSectionModal';
import { IconChevronRight, IconAdd, IconEdit, IconAddBig } from '../utils/icons';

const BusinessPage: React.FC = () => {

    const SliderNextArrow = (props: any) => {
        const { onClick } = props;
        return (
            <div onClick={onClick} className="absolute top-0 bottom-0 right-[-50px] my-auto w-[32px] h-[32px]">
                <Image src={IconChevronRight} fill alt="" />
            </div>
        );
    }

    const settings = {
        dots: false,
        infinite: false,
        variableWidth: true,
        centerPadding: '16px',
        arrow: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <SliderNextArrow />,
    };

    const [sections, setSections] = useState([])
    const [openEditSectionModal, setOpenEditSectionModal] = useState(false);
    const [openAddSectionModal, setOpenAddSectionModal] = useState(false);
    const [activeSection, setActiveSection] = useState<any>(null)

    const handleEditSectionModal = (action: boolean) => {
        setOpenEditSectionModal(action)
    }

    const handleAddSectionModal = (action: boolean) => {
        setOpenAddSectionModal(action)
    }

    const handleDeleteSection = async (uuid: string) => {
        const response = await deleteContentSection(uuid)
        if (response.status) {
            fetchContentSections()
        }
    }

    const currentDate = new Date();
    const currentTime = format(currentDate, 'hh:mm a');

    const fetchContentSections = async () => {
        const response = await getContentSections()
        setSections(response.data)
    }

    useEffect(() => {
        fetchContentSections()
    }, [])


    return (
        <>
            <main className="bg-white">
                <Header />
                <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                    <div className="flex justify-between items-center mb-[10px]">
                        <div className="flex gap-[40px]">
                            <Link href="/business" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold ">Empresas</Link>
                            <Link href="/leads" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Iniciativas</Link>
                            <Link href="/contents" className="text-[24px] text-[#410B9A] px-[13px] font-bold border-b-[2px] border-[#410B9A]">Contenido</Link>
                            <Link href="/users" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Usuarios</Link>
                        </div>
                        <div>
                            <p className="text-[18px] text-[#3A1477] font-bold">3 Secciones</p>
                            <p className="text-[12px] text-[#B1ADC8]">Actualizado {currentTime}</p>
                        </div>
                    </div>
                    <div className="flex justify-end mb-[20px]">
                        <button
                            className="w-[238px] h-[42px] flex items-center justify-center text-[14px] text-white leading-[22px] gap-[8px] block rounded-[40px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F]"
                            onClick={() => {
                                handleAddSectionModal(true)
                            }}
                        >
                            <Image className="w-[24px] h-[24px]" src={IconAdd} alt="" />
                            <span>Agregar una sección</span>
                        </button>
                    </div>

                    {sections?.length > 0 && sections.map((item: any) => (
                        <div key="item.id" className="section">
                            <div className="relative mb-[20px]">
                                <div className="flex justify-between">
                                    <div>
                                        <p className="text-[22px] text-[#3A1477] font-bold">{item.title}</p>
                                        <p className="text-[12px] text-[#B1ADC8]">{item.contents_total} videos</p>
                                    </div>
                                    {item.contents?.length > 0 ? (
                                        <button
                                            className="flex items-center justify-center w-[45px] h-[45px] bg-[#D5C7E8] rounded-full"
                                            onClick={() => {
                                                setActiveSection(item)
                                                handleEditSectionModal(true)
                                            }}
                                        >
                                            <Image src={IconEdit} alt="" />
                                        </button>
                                    ) : (
                                        <MenuSectionModal
                                            editCallback={() => {
                                                setActiveSection(item)
                                                handleEditSectionModal(true)
                                            }}
                                            deleteCallback={() => {
                                                handleDeleteSection(item.id)
                                                fetchContentSections()
                                            }}
                                        />
                                    )}
                                </div>
                                <p className="text-[16px] text-[#353344]">{item.description}</p>
                            </div>

                            <div className="pb-[40px]">
                                <div className="grid grid-cols-[157px_calc(100%-157px)] gap-[16px] relative">
                                    <Link href={`/sections/${item.id}/contents/create`} className="flex w-full items-center justify-center min-w-[157px] max-w-[157px] h-[187px] rounded-[16px] border-[2px] border-dashed border-[#B1ADC8]">
                                        <Image className="w-[59px] h-[59px]" src={IconAddBig} alt="" />
                                    </Link>
                                    <div className="w-full">
                                        {item.contents?.length > 0 && (
                                            <Slider {...settings}>
                                                {item.contents.map((data: any) => (
                                                    <Link key="data.id" href={`/contents/${data.id}`}>
                                                        <div className="relative w-[157px] h-[187px] rounded-[16px] mr-[16px] bg-gradient overflow-hidden">
                                                            <Image className="block object-cover" fill src={data.banner_url} alt="" />
                                                            <div className="absolute bottom-[10px] left-[10px] z-10">
                                                                <p className="text-white text-[12px] font-bold leading-[18px]">{data.title}</p>
                                                            </div>
                                                        </div>
                                                    </Link>
                                                ))}
                                            </Slider>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}

                </div>
            </main>
            {openAddSectionModal && (
                <AddSectionModal callback={() => {
                    handleAddSectionModal(false)
                    fetchContentSections()
                }} />
            )}
            {openEditSectionModal && (
                <EditSectionModal section={activeSection} callback={() => {
                    handleEditSectionModal(false)
                    fetchContentSections()
                }} />
            )}
        </>
    );
};

export default BusinessPage;