'use client'

import React, { useEffect, useState } from 'react';
import { formatDistanceToNow } from 'date-fns';
import Image from "next/image";
import Lead from '@/assets/images/lead.png'
import Thumb from '@/assets/images/thumb.jpg'
import Profile from '@/assets/images/profile.jpg'
import Header from '@/components/shared/header';
import { getLeadById, getLeadPostsByLeadId } from '@/api/leads';
import CancelLeadModal from '@/components/modals/cancelLeadModal';
import { format } from 'date-fns';
import { limitText } from '@/helpers/utils';
import { es } from "date-fns/locale";

interface EditProps { 
    params: { uuid: string } 
}

const LeadEdit: React.FC<EditProps> = ({ params }) => {

    const [activeTab, setActiveTab] = useState('description');
    const [lead, setLead] = useState<any>(null);
    const [leadPosts, setLeadPosts] = useState<any>([]);
    const [openCancelModal, setOpenCancelModal] = useState(false);

    const handleTabClick = (tabName: string) => {
        setActiveTab(tabName)
    }

    const handleCancelModal = (action: boolean) => {
        setOpenCancelModal(action)
    }

    const fetchLeadById = async () => {
        const response = await getLeadById(params.uuid)
        setLead(response.data)
    }

    const fetchLeadPostByLeadId = async () => {
        const response = await getLeadPostsByLeadId(params.uuid)
        setLeadPosts(response.data)
    }


    const progressBar = (total: number, limit:  number) => {
        return (total / limit) * 100 + '%'
    }

    useEffect(() => {
        fetchLeadById()
        fetchLeadPostByLeadId()
    }, [])

    return (
        <>
            <main className="bg-white">
                <Header />
                {lead && (
                    <div className="container  lg:max-w-[1010px] max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[66px] pb-[80px]">
                        <div className="flex justify-between mb-[12px]">
                            <div className="w-full max-w-[600px]">
                                <div className="w-full h-[330px] rounded-[8px] mb-[6px]">
                                    <Image className="w-full h-full object-cover rounded-[15px]" width={600} height={330} src={lead.banner_url} alt="" />
                                </div>
                                <div className="py-[15px]">
                                    <h2 className="text-[22px] text-[#3A1477] leading-[28px] font-bold">{lead.name}</h2>
                                </div>
                            </div>
                            <div className="w-full pl-[20px] max-w-[380px]">
                                <div className="mb-[27px]">
                                    <p className="text-[#B1ADC8] text-[14px] leading-[21px] mb-[8px]">Iniciativa creada por</p>
                                    <div className="flex items-center ">
                                        <div className="rounded-full overflow-hidden w-[34px] h-[34px] mr-[8px]">
                                            <Image className="block" width={34} height={34} src={Profile} alt="" />
                                        </div>
                                        <div>
                                            <p className="text-[#191D2F] text-[14px] leading-[21px] mb-0">{lead.user?.name + " " + lead.user?.last_name}</p>
                                            <p className="text-[#777777] text-[10px] leading-[15px] mb-0">{lead.user.position}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="mb-[47px]">
                                    <p className="text-[#B1ADC8] text-[14px] mb-[4px]">Temas relacionados</p>
                                    <div className="flex gap-[10px]">
                                        <span className="inline-block border-[#410B9A] border-2 rounded-[12px] text-[#410B9A] text-[12px] leading-[18px] font-medium px-[12px] py-[8px]">{lead.lead_category.name}</span>
                                    </div>
                                </div>
                                {lead.status !== "CANCELLED" && (
                                    <button
                                        className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] text-white bg-gradient-to-r from-[#6C0BE8] to-[#191D2F]"
                                        onClick={() => handleCancelModal(true)}
                                    >
                                        Cancelar iniciativa
                                    </button>
                                )}
                            </div>
                        </div>

                        <div className="flex justify-between">
                            <div className="w-full max-w-[600px]">
                                <div className="flex gap-[70px] mb-[30px]">
                                    <button className={`text-[16px] leading-[24px] ${activeTab === 'description' ? 'text-[#6D04F2] border-b-[2px] border-[#6D04F2]' : 'text-[#D5C7E8]'} px-[13px]`} onClick={() => handleTabClick('description')}>Descripción</button>
                                    <button className={`text-[16px] leading-[24px] ${activeTab === 'latest' ? 'text-[#6D04F2] border-b-[2px] border-[#6D04F2]' : 'text-[#D5C7E8]'} px-[13px]`} onClick={() => handleTabClick('latest')}>Últimas actividades</button>
                                </div>
                                {activeTab === 'description' && (
                                    <div className="w-full text-[#191D2F] text-[16px] leading-[24px]">
                                        <p>{ lead.description }</p>
                                    </div>
                                )}
                                {activeTab === 'latest' && (
                                    <>
                                        { leadPosts?.length > 0 && leadPosts.map((item: any) => (
                                            <div key="item.id" className="flex gap-[20px] mb-[15px]">
                                                { item.media_url && (
                                                    <div className="relative w-full max-w-[202px] min-h-[114px] rounded-[8px] overflow-hidden">
                                                        <Image width={202} height={166} className="w-full h-full object-cover" src={item.media_url} alt="" />
                                                    </div>
                                                )}
                                                <div className="w-full relative">
                                                    <p className="text-[16px] text-[#353344] leading-[24px]">{ limitText(item.body, 150) }</p>
                                                    <span className="text-[12px] text-[#B1ADC8] leading-[18px] absolute bottom-0 right-0">{ formatDistanceToNow(new Date(item.created_at), { addSuffix: true, locale: es }) }</span>
                                                </div>
                                            </div>
                                        ))}
                                    </>
                                )}
                            </div>

                            <div className="w-full max-w-[380px] h-[272px] border-l border-l-[#6D04F2]">
                                <div className="max-w-[274px] pl-[24px] mb-[30px]">
                                    <p className="text-[#B1ADC8] text-[14px] leading-[21px] mb-[4px]">Firmas</p>
                                    <div className="flex items-center pt-[5px] pb-[8px]">
                                        <div className="w-full h-[6px] bg-gray-200 rounded-full">
                                            <div className="h-full rounded-full bg-bar"
                                            style={{ width: progressBar(lead.signature_total, lead.signature_limit) }}></div>
                                        </div>
                                    </div>
                                    <div className="flex justify-between">
                                        <span className="text-[12px] leading-[18px] font-bold">Por ahora: {lead.signature_total}</span>
                                        <span className="text-[12px] leading-[18px] font-bold">Misión: {lead.signature_limit} </span>
                                    </div>
                                </div>
                                <div className="max-w-[274px] pl-[24px] mb-[40px]">
                                    <p className="text-[#B1ADC8] text-[14px] leading-[21px] mb-[4px]">Ubicación</p>
                                    <p className="text-[#353344] text-[14px]">{lead.city}, {lead.municipality}</p>
                                </div>
                                <div className="max-w-[274px] pl-[24px]">
                                    <p className="text-[#B1ADC8] text-[14px] leading-[21px] mb-[4px]">Tiempo para lograrlo</p>
                                    <p className="text-[#353344] text-[12px]">{ format(new Date(lead.created_at), 'dd/MM/yyyy') } - { format(new Date(lead.deadline), 'dd/MM/yyyy') }</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </main>

            { openCancelModal && (
                <CancelLeadModal uuid={params.uuid} callback={() => {
                    handleCancelModal(false)
                    fetchLeadById()
                }}/>
            )}
        </>
    );
};

export default LeadEdit;