'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import Profile from '@/assets/images/profile.jpg'
import Link from 'next/link';
import Header from '@/components/shared/header';
import { getLeads } from '@/api/leads';
import { format } from 'date-fns';
import oops from '@/assets/images/oops.svg'
import dog from '@/assets/images/dog.jpg'
import { IconLoad } from '../utils/icons';
import PercentCircle from '@/components/elements/percentCircle';

const BusinessPage: React.FC = () => {

    const [loading, setLoading] = useState(true)
    const [leads, setLeads] = useState([])

    const statusLabels: any = {
        ACTIVE: 'Activa',
        CANCELLED: 'Cancelada',
        MODIFIED: 'Modificada',
        FINISHED: 'Finalizada',
        DELETED: 'Eliminada',
    };

    const currentDate = new Date();
    const currentTime = format(currentDate, 'hh:mm a');

    const fetchLeads = async () => {
        const response = await getLeads()
        setLeads(response.data)
        setLoading(false)
    }

    const progressBar = (total: number, limit:  number) => {
        return (total / limit) * 100
    }
    
    useEffect(() => {
        fetchLeads()
    }, [])

    return (
        <div className="bg-white">
            <Header />
            <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                <div className="flex justify-between items-center mb-[55px]">
                    <div className="flex gap-[40px]">
                        <Link href="/business" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold ">Empresas</Link>
                        <Link href="/leads" className="text-[24px] text-[#410B9A] px-[13px] font-bold border-b-[2px] border-[#410B9A]">Iniciativas</Link>
                        <Link href="/contents" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Contenido</Link>
                        <Link href="/users" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Usuarios</Link>

                    </div>
                    <div>
                        <p className="text-[18px] text-[#3A1477] font-bold">{leads?.length} Iniciativas</p>
                        <p className="text-[12px] text-[#B1ADC8]">Actualizado {currentTime}</p>
                    </div>
                </div>

                { !loading && (
                    leads?.length > 0 ? (
                        <div className="grid grid-cols-[repeat(auto-fit,222px)] gap-[40px] pb-[40px]">
                            {leads?.map((item: any) => (
                                <Link key={item.id} href={`/leads/${item.id}`}>
                                    <figure className="max-w-[222px] w-full h-full overflow-hidden rounded-[16px] shadow-md">
                                        <div className="relative max-h-[150px] overflow-hidden">
                                            <Image className="block" width={300} height={200} src={item.banner_url} alt="" />
                                            <span className={`absolute top-0 right-0 rounded-bl-[16px] rounded-tr-[16px] ${item.status === "ACTIVE" ? 'bg-[#2E8F00]' : (item.status === "FINISHED" ? 'bg-[#6D04F2]' : 'bg-[#69657E]')} px-[16px] py-[4px] text-[10px] text-white font-bold`}>
                                                {statusLabels[item.status]}
                                            </span>
                                        </div>
                                        <figcaption className="relative px-[16px] pb-[15px] z-[1]">
                                            <div className="mb-[12px]">
                                                <h3 className={`text-[12px] mt-[10px] ${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#410B9A]'} font-bold`}>{item.name}</h3>
                                            </div>
                                            <div className="px-[2px]">
                                                <div className="flex justify-start gap-[8px] mb-[6px] items-center">
                                                    <div className={`rounded-full bg-[#d5c7e8] ${item.status === "CANCELLED" ? 'grayscale opacity-50' : ''} overflow-hidden relative min-w-[25px] h-[25px] border-[#F2C930] border-[2px]`}>
                                                        { item.user.profile_picture_url && (
                                                            <Image className="block" fill src={item.user.profile_picture_url} alt="" />
                                                        )}
                                                    </div>
                                                    <div className="">
                                                        <p className={`${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#353344]'} text-[12px] leading-[12px] mb-0`}>{item.user?.name + " " + item.user?.last_name}</p>
                                                    </div>
                                                </div>
                                                <div className="mb-[12px]">
                                                    <p className="text-[#B1ADC8] text-[10px]">Categoría</p>
                                                    <p className={`${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#353344]'} text-[12px]`}>{item.lead_category.name}</p>
                                                </div>
                                                <div className="flex flex-between gap-[22px] mb-[12px]">
                                                    <div>
                                                        <p className="text-[#B1ADC8] text-[10px]">Firmas</p>
                                                        <div className="flex items-center">
                                                            <PercentCircle className={`${item.status === "CANCELLED" ? 'grayscale opacity-30' : ''} ml-[-3px] mr-[2px] block`} percent={progressBar(item.signature_total, item.signature_limit)} diameter={23} />
                                                            <p className={`${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#353344]'} text-[12px]`}>{item.signature_total} de {item.signature_limit}</p>
                                                        </div>
                                                    </div>
                                                    <div className={`block h-[36px] w-[1px] rounded-[1px] ${item.status === "CANCELLED" ? 'bg-[#B1ADC8]' : 'bg-[#6D04F2]'}`}></div>
                                                    <div>
                                                        <p className="text-[#B1ADC8] text-[10px]">Ubicación</p>
                                                        <p className={`${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#353344]'} text-[12px]`}>{item.city}</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    <p className="text-[#B1ADC8] text-[10px]">Tiempo para logarlo</p>
                                                    <p className={`${item.status === "CANCELLED" ? 'text-[#B1ADC8]' : 'text-[#353344]'} text-[12px] text-[12px]`}>{format(new Date(item.created_at), 'dd/MM/yyyy')} - {format(new Date(item.deadline), 'dd/MM/yyyy')}</p>
                                                </div>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </Link>
                            ))}
                        </div>
                    ) : (
                        <div className="flex flex-col p-[60px] items-center justify-center">
                            <div className="mb-[24px]">
                                <Image src={oops} alt="" />
                            </div>
                            <div className="mb-[24px] text-center">
                                <h3 className="text-[24px] font-bold text-[#410B9A] mb-[8px]">Aún no se ha dado de alta ninguna iniciativa</h3>
                                <p className="text-[16px] text-[#353344]">Explicación / Sugerencia</p>
                            </div>
                            <div>
                                <Image src={dog} alt="" />
                            </div>
                        </div>
                    )
                )}
            </div>
        </div>
    );
};

export default BusinessPage;