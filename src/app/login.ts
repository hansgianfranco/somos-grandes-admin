import { setLocalStorageItem } from "@/helpers/localStorage"

const headers = {
  Accept: "application/json, text/plain, */*",
  "Content-Type": "application/json",
}

export const authLogin = async (user: any) => {
  return await fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/admin/login`, {
    method: "POST",
    headers: {
      ...headers,
    },
    body: JSON.stringify(user),
  })
    .then((response) => response.json())
    .then(async (data) => {
      let result: any = {}
      result = { ...data }
      if (data.status) {
        const { user, access_token } = data
        setLocalStorageItem('sgAccessToken', access_token)
        setLocalStorageItem('sgUserData', JSON.stringify(user))
      }
      return result
    })
    .catch((error) => {
      console.log(error)
      return false
    })
}
