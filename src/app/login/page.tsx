'use client'

import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Image from 'next/image';
import Logo from '@/assets/images/logo.svg';
import { authLogin } from '@/app/login';
import { useRouter } from 'next/navigation'
import { setAuthenticatedCookie } from '../utils/auth';

const LoginPage = () => {

    const router = useRouter()

    const initialValues = {
        email: '',
        password: '',
    };

    const validationSchema = Yup.object({
        email: Yup.string().email('Formato de email inválido').required('Campo requerido'),
        password: Yup.string().required('Campo requerido'),
    });

    const handleSubmit = async (values: any, { setSubmitting }: any) => {
        try {
            const response = await authLogin(values);
            if (response.status) {
                setAuthenticatedCookie();
                router.push('/business'); // Redirect to /business
            } else {
                console.error('Error al iniciar sesión');
            }
        } catch (error) {
            console.error('Error al enviar el formulario:', error);
        } finally {
            setSubmitting(false);
        }
    };
    
    return (
        <main className="flex min-h-screen bg-login justify-center items-center p-24">
            <div className="z-10 max-w-[523px] w-full font-mono text-sm">
                <div className="flex flex-col items-center mb-[37px]">
                    <Image className="mb-[10px]" src={Logo} alt="" />
                    <h2 className="text-[#F2C930] text-[34px]">SOMOS GRANDES</h2>
                </div>
                <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
                    {({ isSubmitting }) => (
                        <Form>
                            <Field
                                className="block w-full h-[60px] text-black pl-[36px] rounded-[16px] mb-[30px]"
                                type="email"
                                name="email"
                                placeholder="Mail"
                            />
                            <ErrorMessage name="email" component="div" className="text-red-500" />

                            <Field
                                className="block w-full h-[60px] text-black pl-[36px] rounded-[16px] mb-[108px]"
                                type="password"
                                name="password"
                                placeholder="Contraseña"
                            />
                            <ErrorMessage name="password" component="div" className="text-red-500" />

                            <div className="flex justify-center">
                                <button
                                    type="submit"
                                    className="w-[238px] h-[42px] flex justify-center items-center block rounded-[40px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F] text-white"
                                    disabled={isSubmitting}
                                >
                                    Iniciar sesión
                                </button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </main>
    );
};

export default LoginPage;