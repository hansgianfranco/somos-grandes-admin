'use client'

import React, { useEffect, useState } from 'react';
import Image from "next/image";
import Link from 'next/link';
import { useDropzone } from 'react-dropzone';
import { uploadUserFile } from '@/api/upload';
import Header from '@/components/shared/header';
import { IconCloudUpload, IconSuccessUpload, IconMedia } from '../utils/icons';

const UserPage: React.FC = () => {
    const [files, setFiles] = useState([]);
    const [uploadProgress, setUploadProgress] = useState(0);
    const [uploadStatus, setUploadStatus] = useState('idle');
    const [uploadError, setUploadError] = useState('');
    const { getRootProps, getInputProps } = useDropzone({
        accept: {
            '.xls, .xlsx, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': []
        },
        onDrop: async (acceptedFiles: any) => {
            setUploadProgress(0)
            const file = acceptedFiles[0];
            setFiles(acceptedFiles.map((file: any) => Object.assign(file, {
                preview: URL.createObjectURL(file)
            })));
            const responseMedia = await uploadUserFile(file, (progressEvent: any) => {
                const progress = Math.round((progressEvent.loaded * 98) / progressEvent.total);
                setUploadProgress(progress);
            });
            if (responseMedia.status) {
                setUploadProgress(100);
                setUploadError('')
            }
            else{
                setUploadProgress(0);
                setUploadStatus('idle');
                setUploadError(responseMedia.error.message)
            }
        }
    });

    useEffect(() => {
        if (uploadProgress === 100 && uploadStatus === 'uploading') {
            setUploadStatus('success');
        } else if (uploadProgress < 100 && uploadProgress > 0 && uploadStatus !== 'uploading') {
            setUploadStatus('uploading');
        }
    }, [uploadProgress, uploadStatus]);

    useEffect(() => {
        return () => files.forEach((file: any) => URL.revokeObjectURL(file.preview));
    }, []);

    return (
        <div className="bg-white">
            <Header />
            <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto pt-[30px]">
                <div className="flex justify-between items-center mb-[55px]">
                    <div className="flex gap-[40px]">
                        <Link href="/business" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Empresas</Link>
                        <Link href="/leads" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Iniciativas</Link>
                        <Link href="/contents" className="text-[24px] text-[#D5C7E8] px-[13px] font-bold">Contenido</Link>
                        <Link href="/users" className="text-[24px] text-[#410B9A] px-[13px] font-bold border-b-[2px] border-[#410B9A]">Usuarios</Link>
                    </div>
                </div>
                <div className="pb-[40px]">
                    <div className="mb-[15px]">
                        <p className="text-[22px] text-[#3A1477] font-bold">Carga masiva de usuarios</p>
                        <p className="text-[12px] text-[#B1ADC8]">Sube un archivo excel</p>
                    </div>
                    { uploadError !== '' && (
                        <p className="text-red-500">Error: {uploadError}</p>
                    )}
                    <div {...getRootProps({ className: 'dropzone w-full min-h-[245px] bg-[#D9D9D9] overflow-hidden rounded-[8px]' })}>
                        <input {...getInputProps()} />
                        {uploadStatus === 'idle' && (
                            <div className="flex flex-col min-h-[245px] w-full h-full items-center justify-center">
                                <Image className="block mb-[10px]" src={IconCloudUpload} alt="" />
                                <p className="text-[18px] text-white leading-[27px] font-medium">Adjuntar archivo</p>
                            </div>
                        )}
                        {uploadStatus === 'uploading' && (
                            <div className="relative pointer-events-none">
                                <div className="absolute top-0 left-0 flex flex-col min-h-[245px] w-full h-full items-center justify-center">
                                    <Image className="block mb-[10px] animate-pulse" src={IconCloudUpload} alt="" />
                                    <p className="text-[18px] text-white leading-[27px] font-medium">Subiendo: {uploadProgress + '%'}</p>
                                </div>
                            </div>
                        )}
                        {uploadStatus === 'success' && (
                            <div className="relative pointer-events-none">
                                <div className="absolute top-0 left-0 flex flex-col min-h-[245px] w-full h-full items-center justify-center">
                                    <Image className="block mb-[10px]" src={IconSuccessUpload} alt="" />
                                    <p className="text-[18px] text-white leading-[27px] font-medium">Éxito</p>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div >
        </div >
    );
};

export default UserPage;