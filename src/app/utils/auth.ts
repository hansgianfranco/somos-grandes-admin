"use server"

import { cookies } from "next/headers"

export const setAuthenticatedCookie = () => {
  const cookieStore = cookies();
  cookieStore.set("isAuthenticated", "true", { 
    path: '/', 
    maxAge: 60 * 60 * 24,
    httpOnly: false,
    sameSite: 'strict'
  });

  const hasCookie = cookieStore.has("isAuthenticated");
}

export const getAuthenticatedCookie = () => {
  const cookieStore = cookies();
  const hasCookie = cookieStore.has("isAuthenticated");
  return cookieStore.get("isAuthenticated");
}