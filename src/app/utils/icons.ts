import IconDescription from "@/assets/images/icons/icon-description.svg"
import IconMission from "@/assets/images/icons/icon-mission.svg"
import IconChallenge from "@/assets/images/icons/icon-challenge.svg"
import IconPride from "@/assets/images/icons/icon-pride.svg"
import IconGoal from "@/assets/images/icons/icon-goal.svg"
import IconLink from "@/assets/images/icons/icon-link.svg"
import IconEmail from "@/assets/images/icons/icon-email.svg"
import IconPhone from "@/assets/images/icons/icon-phone.svg"
import IconMap from "@/assets/images/icons/icon-map.svg"
import IconDown from "@/assets/images/icons/icon-down.svg"
import IconLinkedin from "@/assets/images/icons/icon-linkedin.svg"
import IconFacebook from "@/assets/images/icons/icon-facebook.svg"
import IconInstagram from "@/assets/images/icons/icon-instagram.svg"
import IconX from "@/assets/images/icons/icon-x.svg"
import IconSave from "@/assets/images/icons/icon-save.svg"
import IconCloudUpload from "@/assets/images/icons/icon-cloud-upload.svg"
import IconMedia from "@/assets/images/icons/icon-media-video.svg"
import IconReturn from "@/assets/images/icons/icon-return.svg"
import IconSuccessUpload from "@/assets/images/icons/icon-success-big.svg"
import IconAddBanner from "@/assets/images/icons/icon-add-banner.svg"
import IconLoad from "@/assets/images/icons/icon-load.svg"
import IconPlus from "@/assets/images/icons/icon-plus.svg"
import IconAdd from "@/assets/images/icons/icon-add.svg"
import IconAddBig from "@/assets/images/icons/icon-add-big.svg"
import IconEdit from "@/assets/images/icons/icon-edit.svg"
import IconChevronRight from "@/assets/images/icons/icon-chevron-right.svg"
import IconDelete from "@/assets/images/icons/icon-delete.svg"
import IconAddChapter from "@/assets/images/icons/icon-add-chapter.svg"
import IconMenu from "@/assets/images/icons/icon-menu.svg"
import IconEditBlack from "@/assets/images/icons/icon-edit-black.svg"
import IconPlusBlack from "@/assets/images/icons/icon-plus-black.svg"
import IconDeleteBlack from "@/assets/images/icons/icon-delete-black.svg"


export {
  IconDescription,
  IconMission,
  IconChallenge,
  IconPride,
  IconGoal,
  IconLink,
  IconEmail,
  IconPhone,
  IconMap,
  IconDown,
  IconLinkedin,
  IconFacebook,
  IconInstagram,
  IconX,
  IconSave,
  IconCloudUpload,
  IconMedia,
  IconReturn,
  IconSuccessUpload,
  IconAddBanner,
  IconLoad,
  IconPlus,
  IconAdd,
  IconAddBig,
  IconEdit,
  IconChevronRight,
  IconDelete,
  IconAddChapter,
  IconMenu,
  IconEditBlack,
  IconPlusBlack,
  IconDeleteBlack
}
