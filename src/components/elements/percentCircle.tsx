import React, { useEffect, useRef } from 'react';



interface PercentCircleProps {
  className?: string;
  percent: number;
  diameter: number;
}

const PercentCircle: React.FC<PercentCircleProps> = ({ percent, diameter, className}) => {
  const circleRef = useRef<SVGCircleElement>(null);

  useEffect(() => {
    if (circleRef.current) {
      const radius = circleRef.current.r.baseVal.value;
      const circumference = 2 * Math.PI * radius;
      const strokeDashArray = (percent * circumference) / 100;

      circleRef.current.style.strokeDasharray = `${strokeDashArray} ${circumference}`;
    }
  }, [percent]);

  const radius = (diameter - 10) / 2; // Ajuste del radio para que se ajuste al SVG con el grosor del trazo

  return (
    <svg
        className={`${className} transform -rotate-45 transition-all duration-1000 ease-in-out`}
        viewBox={`0 0 ${diameter} ${diameter}`}
        width={diameter}
        height={diameter}
        data-percent={percent}
      >
        <circle
          cx={diameter / 2}
          cy={diameter / 2}
          r={radius}
          fill="none"
          stroke={'#d5c7e8'}
          strokeWidth="2"
        />
        <circle
          ref={circleRef}
          cx={diameter / 2}
          cy={diameter / 2}
          r={radius}
          fill="none"
          stroke={'#6D04F2'}
          strokeWidth="2"
          strokeDasharray="0 999"
        />
      </svg>
  );
};

export default PercentCircle;