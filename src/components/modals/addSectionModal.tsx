import React from 'react';
import Image from "next/image";
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import { postContentSection } from '@/api/contents';
import { IconSave } from '@/app/utils/icons';

interface ModalProps {
    callback?: () => void;
}

const validationSchema = Yup.object().shape({
    title: Yup.string().required(),
    description: Yup.string().required('Por favor selecciona un motivo.'),
});

const AddSectionModal: React.FC<ModalProps> = ({ callback }) => {

    const handleSubmit = async (values: any) => {
        const response = await postContentSection(values);
        if (response.status) {
            callback && callback();
        }
    };
    
    return (
        <div className="w-screen h-screen fixed top-0 left-0 right-0 bottom-0 flex justify-center items-center bg-[#1F273C]/70 z-10">
            <div className="max-w-[698px] w-full px-[99px] py-[46px] bg-white rounded-[16px]">
                <h3 className="text-[#410B9A] text-[16px] leading-[24px] font-bold mb-[31px]">Agregar sección</h3>

                <Formik
                    initialValues={{ title: '', description: '' }}
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                >
                    <Form>
                        <div className="mb-[36px]">
                            <Field
                                name="title"
                                className="border border-[#6D04F2] pl-[18px] w-full h-[56px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                type="text"
                                placeholder="Titulo"
                            />
                        </div>
                        <div className="mb-[34px]">
                            <Field
                                name="description"
                                as="textarea"
                                className="border border-[#6D04F2] p-[18px] w-full h-[181px] rounded-[4px] text-[16px] text-[#353344] leading-[24px]"
                                placeholder="Descripcion"
                            />
                            <span className="text-[12px] text-[#B1ADC8] leading-[18px]">0/300</span>
                        </div>
                        <div className="flex justify-center items-center gap-[24px]">
                            <button
                                type="button"
                                className="w-[278px] h-[40px] flex justify-center items-center block rounded-[40px] text-[14px] text-[#410B9A] leading-[22px] border border-[#6D04F2]"
                                onClick={() => callback && callback()}
                            >
                                Cancelar
                            </button>
                            <button
                                type="submit"
                                className="w-[278px] h-[40px] flex justify-center items-center block rounded-[40px] gap-[8px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F] text-[14px] text-white leading-[22px]"
                            >
                                <Image className="w-[24px] h-[24px]" src={IconSave} alt="" />
                                <span>Guardar cambios</span>
                            </button>
                        </div>
                    </Form>
                </Formik>
            </div>
        </div>
    );
};

export default AddSectionModal;