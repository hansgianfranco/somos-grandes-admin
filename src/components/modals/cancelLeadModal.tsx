'use client'

import React, { useState } from 'react';
import Image from "next/image";
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { updateLeadByStatus } from '@/api/leads';
import Cat from '@/assets/images/cat.jpg'

const options = [
    {
        id: 'country-option-1',
        value: 'Infracción en la normativa sobre Contenido Sexual y Lenguaje Inapropiado',
        label: 'Infracción en la normativa sobre Contenido Sexual y Lenguaje Inapropiado',
    },
    {
        id: 'country-option-2',
        value: 'Infracción en la normativa de Seguridad Infantil',
        label: 'Infracción en la normativa de Seguridad Infantil',
    },
    {
        id: 'country-option-3',
        value: 'Contenido Sensible Relacionado con el Suicidio y Trastornos Alimentarios',
        label: 'Contenido Sensible Relacionado con el Suicidio y Trastornos Alimentarios',
    },
    {
        id: 'country-option-4',
        value: 'Infracción en la normativa sobre la Prohibición de Armas de Fuego',
        label: 'Infracción en la normativa sobre la Prohibición de Armas de Fuego',
    },
    {
        id: 'country-option-5',
        value: 'Infracción en la normativa de Prohibición de la Incitación al Odio y la Discriminación',
        label: 'Infracción en la normativa de Prohibición de la Incitación al Odio y la Discriminación',
    },
    {
        id: 'country-option-6',
        value: 'Infracción en la normativa sobre Prohibición de Organizaciones Criminales o Extremistas Violentas',
        label: 'Infracción en la normativa sobre Prohibición de Organizaciones Criminales o Extremistas Violentas',
    },
];

interface ModalProps {
    uuid: string;
    callback?: () => void;
}

const validationSchema = Yup.object().shape({
    status: Yup.string().required(),
    deleted_reason: Yup.string().required('Por favor selecciona un motivo.'),
});

const CancelLeadModal: React.FC<ModalProps> = ({ uuid, callback }) => {

    const [first, setFirst] = useState<boolean>(true)

    const handleSubmit = async (values: any) => {
        const response = await updateLeadByStatus(uuid, values);
        if (response.status) {
            setFirst(true);
            callback && callback();
        }
    };

    return (
        <div className="w-screen h-full fixed top-0 left-0 right-0 bottom-0 p-[30px] flex justify-center flex-wrap flex-grow overflow-auto items-center bg-[#1F273C]/70 z-10">
            <div className="max-w-[725px] w-full px-[32px] py-[31px] bg-white rounded-[16px]">
                <Formik
                    initialValues={{
                        status: 'CANCELLED',
                        deleted_reason: ''
                    }}
                    validateOnChange={true}
                    validateOnBlur={true}
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                >
                    {formik => (
                        <Form>
                            <div className={`${first ? '' : 'hidden'}`}>
                                <h3 className="text-center text-[#410B9A] text-[24px] leading-[31px] font-bold mb-[40px]">Selecciona un motivo por el cual será cancelada esta iniciativa</h3>
                                <fieldset className="mb-[40px]">
                                    {options.map(option => (
                                        <div key={option.id} className="flex items-center box-shadow rounded-[8px] px-[22px] py-[18px] mb-4">
                                            <Field
                                                type="radio"
                                                id={option.id}
                                                name="deleted_reason"
                                                value={option.value}
                                                className={`w-4 h-4 ${formik.errors.deleted_reason ? 'border-red' : 'border-gray-300'} focus:ring-2 focus:ring-blue-300`}
                                            />
                                            <label htmlFor={option.id} className="block ms-2 text-sm font-medium text-[#69657E]">
                                                {option.label}
                                            </label>
                                        </div>
                                    ))}
                                    {formik.errors.deleted_reason && (
                                        <div className="text-red-500">{formik.errors.deleted_reason}</div>
                                    )}
                                </fieldset>
                                <div className="flex justify-center flex-col items-center gap-[8px]">
                                    <button
                                        type="button"
                                        className="w-[278px] h-[40px] flex justify-center items-center block rounded-[40px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F] text-white"
                                        onClick={() => {
                                            formik.validateForm()
                                            if (formik.isValid && formik.values.deleted_reason !== '') {
                                                setFirst(false)
                                            }
                                        }}
                                    >
                                       Cancelar iniciativa
                                    </button>
                                    <button type="button" className="w-[278px] h-[40px] flex justify-center items-center block text-[#410B9A]" onClick={() => callback && callback()}>Salir</button>
                                </div>
                            </div>
                            <div className={`${first ? 'hidden' : ''}`}>
                                <h3 className="text-center text-[#410B9A] text-[24px] leading-[31px] font-bold mb-[8px]">¿Estás seguro de <br /> de cancelar la iniciativa?</h3>
                                <p className="text-center text-[#191D2F] text-[16px] leading-[24px] mb-[40px]">Al cancelar una iniciativa, esta ya podrá ser vista en la aplicación.</p>
                                <div className="flex justify-center mb-[40px]">
                                    <Image className="w-[268px] h-[227px]" src={Cat} alt="" />
                                </div>
                                <div className="flex justify-center flex-col items-center gap-[8px]">
                                    <button
                                        className="w-[278px] h-[40px] flex justify-center items-center block rounded-[40px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F] text-white"
                                        type="submit"
                                    >
                                       Cancelar iniciativa
                                    </button>
                                    <button
                                        className="w-[278px] h-[40px] flex justify-center items-center block text-[#410B9A]"
                                        onClick={() => {
                                            callback && callback();
                                            setFirst(true);
                                        }}
                                    >
                                        Cancelar
                                    </button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    );
};

export default CancelLeadModal;