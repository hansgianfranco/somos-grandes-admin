'use client'

import Image from "next/image";
import Cat from '@/assets/images/cat.jpg'
import { deleteContentChapter } from "@/api/contents";

interface ModalProps {
    uuid: string;
    callback?: () => void;
}

const DeleteChapterModal: React.FC<ModalProps> = ({ uuid, callback }) => {

    const handleSubmit = async () => {
        const response = await deleteContentChapter(uuid)
        if (response.status) {
            callback && callback();
        }
    };

    return (
        <div className="w-screen h-full fixed top-0 left-0 right-0 bottom-0 p-[30px] flex justify-center flex-wrap flex-grow overflow-auto items-center bg-[#1F273C]/70 z-10">
            <div className="max-w-[725px] w-full px-[32px] py-[31px] bg-white rounded-[16px]">
                <h3 className="text-center text-[#410B9A] text-[24px] leading-[31px] font-bold mb-[8px]">¿Estás seguro de <br /> eliminar este capítulo ?</h3>
                <p className="text-center text-[#191D2F] text-[16px] leading-[24px] mb-[40px]">Si continuas con esta acción, los usuarios de la aplicación <br /> ya no podrán tener accesos al material</p>
                <div className="flex justify-center mb-[40px]">
                    <Image className="w-[268px] h-[227px]" src={Cat} alt="" />
                </div>
                <div className="flex justify-center flex-col items-center gap-[8px]">
                    <button
                        className="w-[278px] h-[40px] flex justify-center items-center block rounded-[40px] bg-gradient-to-r from-[#6C0BE8] to-[#191D2F] text-white"
                        onClick={() => {
                            handleSubmit()
                        }}
                    >
                        Eliminar
                    </button>
                    <button
                        className="w-[278px] h-[40px] flex justify-center items-center block text-[#410B9A]"
                        onClick={() => {
                            callback && callback();
                        }}
                    >
                        Cancelar
                    </button>
                </div>
            </div>
        </div>
    );
};

export default DeleteChapterModal;