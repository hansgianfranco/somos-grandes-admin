import React, { useState } from 'react';
import Image from "next/image";
import { IconEditBlack, IconPlusBlack, IconMenu, IconDeleteBlack } from '@/app/utils/icons';

interface ModalProps {
    editCallback: () => void;
    addCallback: () => void;
    deleteCallback: () => void;
}

const MenuContentModal: React.FC<ModalProps> = ({ editCallback, addCallback, deleteCallback }) => {

    const [showModal, setShowModal] = useState(false);

    const handleOpenModal = () => {
        setShowModal(true);
      };
    
      const handleOutsideClick = (event: any) => {
        if (!event.target.closest('.modal-menu')) {
            setShowModal(false);
        }
      };
    
    return (
        <>
            {showModal && (
                <div className="modal-menu absolute w-full max-w-[357px] top-[19px] right-[102px] z-[2] pt-[32px] px-[24px] pb-[40px] bg-[#FFFFFF] rounded-[16px] box-shadow" onClick={handleOutsideClick}>
                    <h3 className="text-[18px] text-[#4F149A] leading-[27px] mb-[24px] font-medium">¿Qué quieres hacer?</h3>
                    <button
                        className="block w-full flex items-center gap-[8px] rounded-[8px] text-left px-[16px] py-[12px] box-shadow-button mb-[2px]"
                        onClick={editCallback}
                    >
                        <Image className="w-[24px] h-[24px]" src={IconEditBlack} alt="" />
                        <span>Editar tema</span>
                    </button>
                    <button
                        className="block w-full flex items-center gap-[8px] rounded-[8px] text-left px-[16px] py-[12px] box-shadow-button mb-[2px]"
                        onClick={addCallback}
                    >
                        <Image className="w-[24px] h-[24px]" src={IconPlusBlack} alt="" />
                        <span>Agregar tema nuevo</span>
                    </button>
                    <button
                        className="block w-full flex items-center gap-[8px] rounded-[8px] text-left px-[16px] py-[12px] box-shadow-button"
                        onClick={deleteCallback}
                    >
                        <Image className="w-[24px] h-[24px]" src={IconDeleteBlack} alt="" />
                        <span>Eliminar</span>
                    </button>
                </div> 
            )}
            <button
                className="absolute top-[17px] right-[33px] flex items-center justify-center w-[45px] h-[45px] bg-[#D5C7E8] rounded-full z-1"
                onClick={() => handleOpenModal()}
            >
                <Image src={IconMenu} alt="" />
            </button>
        </>

    );
};

export default MenuContentModal;