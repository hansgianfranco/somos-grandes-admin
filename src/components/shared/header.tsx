'use client'

import Image from "next/image";
import Link from 'next/link';
import Logo from '@/assets/images/logo-min.svg';
import Profile from '@/assets/images/profile.jpg';
import { IconDown } from "@/app/utils/icons";

const Header: React.FC = () => {
    const user = {
        name: 'ejemplo'
    }
    return (
        <header className="h-[59px] flex items-center box-shadow-header">
            <div className="container max-w-[1024px] xl:max-w-[1280px] mx-auto flex justify-between items-center">
                <Link href="/business">
                    <div className="w-[127px] h-[26px]">
                        <Image className="w-[127px] h-[26px]" src={Logo} alt="Logo" />
                    </div>
                </Link>
                {user && (
                    <div className="flex items-center">
                        <div className="border-[#A7A5A6] rounded-full overflow-hidden w-[32px] border-[3px] h-[32px] mr-[14px]">
                            <Image width={32} height={32} src={Profile} alt="Profile" />
                        </div>
                        <p className="text-[16px] text-[#0E0E0E] font-medium">{user.name}</p>
                        <button className="flex items-center justify-center w-[24px] h-[24px]">
                            <Image width={11} height={6} src={IconDown} alt="Dropdown Icon" />
                        </button>
                    </div>
                )}
            </div>
        </header>
    );
};

export default Header;