'use server'

import { cookies } from 'next/headers';

const cookieStore = cookies();

export const setCookie = (name: string, value: string, options?: object) => {
  cookieStore.set(name, value, options);
};

export const getCookie = (name: string) => {
  return cookieStore.get(name);
};