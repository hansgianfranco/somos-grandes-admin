export const setLocalStorageItem = (key: string, value: any) => {
  try {
    if (typeof window !== "undefined") {
      localStorage.setItem(key, JSON.stringify(value))
    }
  } catch (error) {
    console.error("Error al establecer el elemento en localStorage:", error)
  }
}

export const getLocalStorageItem = (key: string) => {
  try {
    if (typeof window !== "undefined") {
      const item = localStorage.getItem(key)
      return item ? JSON.parse(item) : null
    }
  } catch (error) {
    console.error("Error al obtener el elemento de localStorage:", error)
    return null
  }
}
