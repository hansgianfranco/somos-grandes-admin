export const limitText = (text: string, limit: number) => {
  if (text.length <= limit) {
    return text 
  } else {
    return text.slice(0, limit) + "..." 
  }
}

export const timeFormat = (time: string) => {
  const parts = time.split(":")
  const hours = parseInt(parts[0])
  const minutes = parseInt(parts[1])
  const seconds = parseInt(parts[2])

  if (hours > 0) {
    return `${hours}:${minutes < 10 ? "0" + minutes : minutes} hr`
  } else {
    return `${minutes < 10 ? "0" + minutes : minutes}:${
      seconds < 10 ? "0" + seconds : seconds
    } min`
  }
}

export const base64ToBlob = (base64Data: string) => {
  const commaIndex = base64Data.indexOf(",")
  const mimeType = base64Data.substring(5, commaIndex)
  const base64Content = base64Data.substring(commaIndex + 1)
  const byteCharacters = atob(base64Content)
  const byteArrays = []
  for (let offset = 0; offset < byteCharacters.length; offset += 512) {
    const slice = byteCharacters.slice(offset, offset + 512)

    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }
    const byteArray = new Uint8Array(byteNumbers)
    byteArrays.push(byteArray)
  }
  const blob = new Blob(byteArrays, { type: mimeType })
  return { blob, contentType: mimeType }
}
