import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';

export function middleware(request: NextRequest) {
  const isAuthenticated = request.cookies.get('isAuthenticated');

   if (request.nextUrl.pathname === '/') {
    if (!isAuthenticated || isAuthenticated.value !== 'true') {
      return NextResponse.redirect(new URL('/login', request.url));
    } else {
      return NextResponse.redirect(new URL('/business', request.url));
    }
  }
  
  if (!isAuthenticated || isAuthenticated.value !== 'true') {
    const protectedPaths = [
      '/sections', 
      '/business', 
      '/chapters', 
      '/contents', 
      '/leads'
    ];
    if (protectedPaths.some(path => request.nextUrl.pathname.startsWith(path))) {
      return NextResponse.redirect(new URL('/login', request.url));
    }
  } else {
    if (request.nextUrl.pathname.startsWith('/login')) {
      return NextResponse.redirect(new URL('/', request.url));
    }
  }
}